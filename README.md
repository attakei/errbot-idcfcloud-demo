# IDCFクラウドをChatbotから扱う(DNS編)

Python製のChatbotフレームワークであるErrbotを用いた、IDCFクラウドのDNSサービスをChatOpsするデモです。

![demo](./errbot-idcfcloud.gif)


## 必要なもの

* SlackチームのBotと、そのトークン
* IDCFクラウドのアカウントと、そのAPIキー＆シークレットキー
* Python 3.x系が動く環境（virtualenvなどで仮想環境を用意することを推奨します）


## 動かすまで

Errbotを準備します。

```bash
$ git clone https://gitlab.com/attakei/errbot-idcfcloud-demo.git
$ cd errbot-idcfcloud-demo
$ pip install -r requirements.txt
```

`.env`ファイルにIDCFクラウドのAPIキーとシークレットキーを記載します。

```bash
IDCF_API_KEY=YOUR_API_KEY
IDCF_SECRET_KEY=YOUR_SECRET_KEY
SLACK_BOT_TOKEN=YOUR_SLACK_TOKEN
```

起動します。

```bash
$ errbot
```

botの状態がオンラインになるのを確認してください。


## Errbotの動き概略

Errbotは「自身へのDM」「自身がいるルームへのメッセージ」をチェックして、
先頭に`!`があるメッセージをコマンドとして検知するようになっています。

例：

```
> !help
```

## このデモで実装してあるコマンド

* ゾーン一覧の表示
    * `!idcf_dns_zones`
    * `!ゾーン一覧`
    * `!ゾーンのリスト`
    *  etc
* ゾーン詳細の表示
    * `!idcf_dns_zone (ゾーンのドメイン)`
    * `!(ゾーンのドメイン)について`
    * `!(ゾーンのドメイン)の情報`
    * etc
* ゾーンの新規追加
    * `!idcf_dns_new_zone (ゾーンのドメイン)`
    * `!(ゾーンのドメイン)用のゾーンを追加してください`
* ゾーンへのレコード追加（gTLDのゾーンのみ、AレコードかCNAMEレコードのみ可能）
    * `!idcf_dns_new_record (レコード) (タイプ) (値)`
    * `!(レコード)の(タイプ)レコードに(値)を追加してください`
