import os
from errbot import BotPlugin, botcmd, arg_botcmd
import time
import hmac
import hashlib
from base64 import b64encode
import requests


class DNSClient(object):
    URL_BASE = 'https://dns.idcfcloud.com'
    BASE_PATH = '/api/v1'

    def __init__(self, api_key, secret_key):
        self.api_key = api_key
        self.secret_key = secret_key
        self.session = requests.Session()
        self.session.headers.update({
            'X-IDCF-APIKEY': self.api_key,
        })

    def generate_signature(self, method, path, expires):
        base_string = '\n'.join([
            method, path, self.api_key, str(expires), ''
        ]).encode()
        hash = hmac.new(self.secret_key.encode(), base_string, hashlib.sha256)
        return b64encode(hash.digest())

    def request(self, method, path, data=None, expires=60):
        expires = int(time.time()) + expires
        path = self.BASE_PATH + path
        url = self.URL_BASE + path
        signature = self.generate_signature(method, path, expires)
        self.session.headers.update({
            'X-IDCF-Expires': str(expires),
            'X-IDCF-Signature': signature,
        })
        if method == 'GET':
            resp = self.session.request(method, url)
        else:
            resp = self.session.request(method, url, json=data)
        return resp.json()

    def fetch_zones(self):
        zones = self.request('GET', '/zones')
        return {zone['name']: zone for zone in zones}

    def fetch_zone(self, zone_uuid):
        zone = self.request('GET', '/zones/' + zone_uuid)
        return zone

    def add_new_zone(self, zone_info):
        zone = self.request('POST', '/zones', zone_info)
        return zone

    def add_new_record(self, zone_uuid, record_info):
        record = self.request('POST', f'/zones/{zone_uuid}/records', record_info)
        return record


class Idcfcloud(BotPlugin):
    """
    IDCF cloud operator
    """

    def activate(self):
        """
        Triggers on plugin activation

        You should delete it if you're not using it to override any default behaviour
        """
        super(Idcfcloud, self).activate()
        self.dns_client = DNSClient(
            os.environ['IDCF_API_KEY'], os.environ['IDCF_SECRET_KEY']
        )
        self.setdefault('zone_info', {})
        self.setdefault('avatar', 'tiara')

    def emoji_avatar(self, prefix=None):
        avatar = self.get('avatar', None)
        if avatar is None:
            return ''
        return ':idcf_{}: '.format(avatar)

    @botcmd(template="zones")
    def idcf_dns_zones(self, msg, match):
        yield self.emoji_avatar() + 'チェック中です...'
        self['zone_info'] = self.dns_client.fetch_zones()
        yield {'zones': self['zone_info']}

    @arg_botcmd('zone_name', type=str, template="zone")
    def idcf_dns_zone(self, msg, zone_name):
        yield self.emoji_avatar() + 'チェック中です...'
        if self['zone_info'] == {}:
            try:
                self['zone_info'] = self.dns_client.fetch_zones()
            except requests.RequestException:
                yield "取得に失敗しました.."
                return
        if zone_name not in self['zone_info']:
            yield "管理していないゾーン名です"
            return
        zone = self['zone_info'][zone_name]
        try:
            zone_info = self.dns_client.fetch_zone(zone['uuid'])
        except requests.RequestException:
            yield "取得に失敗しました.."
            return
        yield {'zone': zone_info}


    @arg_botcmd('zone_name', type=str, template="zone_created")
    def idcf_dns_new_zone(self, msg, zone_name=None):
        yield self.emoji_avatar() + 'チェック中です...'
        if self['zone_info'] == {}:
            try:
                self['zone_info'] = self.dns_client.fetch_zones()
            except requests.RequestException:
                yield "管理済みゾーンの取得に失敗しました.."
                return
        if zone_name in self['zone_info']:
            yield "すでに登録済みのゾーンです"
            return
        zone = self.dns_client.add_new_zone({
            "name": zone_name,
            "email": "attakei@gmail.com",
            "description": "my zone",
            "default_ttl": 30000,
        })
        yield {'zone': zone}
        return

    @arg_botcmd('r_value', type=str)
    @arg_botcmd('r_type', type=str)
    @arg_botcmd('r_name', type=str)
    def idcf_dns_new_record(self, msg, r_name=None, r_type=None, r_value=None):
        yield self.emoji_avatar() + 'チェック中です...'
        if self['zone_info'] == {}:
            try:
                self['zone_info'] = self.dns_client.fetch_zones()
            except requests.RequestException:
                yield "取得に失敗しました.."
                return
        zone_name = '.'.join(r_name.split('.')[-2:])
        if zone_name not in self['zone_info']:
            yield "管理していないゾーン名ですね"
            return
        zone_info = self['zone_info'][zone_name]
        record = self.dns_client.add_new_record(
            zone_info['uuid'],
            {
                "name": r_name + '.',
                "type": r_type,
                "content": r_value,
                "ttl": zone_info['default_ttl'],
            })
        yield '作成しました'
