{{ zone.name }}の情報です

* description: {{ zone.description }}

レコード情報です

{% for record in zone.records -%}
* {{ record.name}}: {{ record.type }}: {{ record.content }}
{% endfor %}
