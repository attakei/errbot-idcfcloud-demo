import os
from errbot import BotPlugin, re_botcmd
import time
import hmac
import hashlib
from base64 import b64encode
import requests


class DNSClient(object):
    URL_BASE = 'https://dns.idcfcloud.com'
    BASE_PATH = '/api/v1'

    def __init__(self, api_key, secret_key):
        self.api_key = api_key
        self.secret_key = secret_key
        self.session = requests.Session()
        self.session.headers.update({
            'X-IDCF-APIKEY': self.api_key,
        })

    def generate_signature(self, method, path, expires):
        base_string = '\n'.join([
            method, path, self.api_key, str(expires), ''
        ]).encode()
        hash = hmac.new(self.secret_key.encode(), base_string, hashlib.sha256)
        return b64encode(hash.digest())

    def request(self, method, path, data=None, expires=60):
        expires = int(time.time()) + expires
        path = self.BASE_PATH + path
        url = self.URL_BASE + path
        signature = self.generate_signature(method, path, expires)
        self.session.headers.update({
            'X-IDCF-Expires': str(expires),
            'X-IDCF-Signature': signature,
        })
        if method == 'GET':
            resp = self.session.request(method, url)
        else:
            resp = self.session.request(method, url, json=data)
        return resp.json()

    def fetch_zones(self):
        zones = self.request('GET', '/zones')
        return {zone['name']: zone for zone in zones}

    def fetch_zone(self, zone_uuid):
        zone = self.request('GET', '/zones/' + zone_uuid)
        return zone

    def add_new_zone(self, zone_info):
        zone = self.request('POST', '/zones', zone_info)
        return zone

    def add_new_record(self, zone_uuid, record_info):
        record = self.request('POST', f'/zones/{zone_uuid}/records', record_info)
        return record


class IDCFcloudJa(BotPlugin):
    """
    IDCF cloud operator
    """

    def activate(self):
        """
        Triggers on plugin activation

        You should delete it if you're not using it to override any default behaviour
        """
        super().activate()
        self.dns_client = DNSClient(
            os.environ['IDCF_API_KEY'], os.environ['IDCF_SECRET_KEY']
        )
        self.setdefault('zone_info', {})
        self.setdefault('avatar', 'tiara')

    def emoji_avatar(self, prefix=None):
        avatar = self.get('avatar', None)
        if avatar is None:
            return ''
        return ':idcf_{}: '.format(avatar)

    @re_botcmd(pattern=r'ゾーンの?(一覧|リスト).*', template="zones")
    def ja_idcf_dns_zones(self, msg, match):
        yield 'チェック中です...'
        self['zone_info'] = self.dns_client.fetch_zones()
        yield {'zones': self['zone_info']}

    @re_botcmd(pattern=r'(?P<zone_name>[0-9a-zA-Z.]+)の?(について|情報).*', template="zone")
    def ja_idcf_dns_zone(self, msg, match):
        zone_name = match.group('zone_name')
        yield 'チェック中です...'
        if self['zone_info'] == {}:
            try:
                self['zone_info'] = self.dns_client.fetch_zones()
            except requests.RequestException:
                yield "取得に失敗しました.."
                return
        if zone_name not in self['zone_info']:
            yield "管理していないゾーン名です"
            return
        zone = self['zone_info'][zone_name]
        try:
            zone_info = self.dns_client.fetch_zone(zone['uuid'])
        except requests.RequestException:
            yield "取得に失敗しました.."
            return
        yield {'zone': zone_info}


    @re_botcmd(pattern=r'(?P<zone_name>[0-9a-zA-Z.]+)用のゾーンを追加してください', template="zone_created")
    def ja_idcf_dns_new_zone(self, msg, match):
        zone_name = match.group('zone_name')
        yield self.emoji_avatar() + 'チェック中です...'
        if self['zone_info'] == {}:
            try:
                self['zone_info'] = self.dns_client.fetch_zones()
            except requests.RequestException:
                yield "管理済みゾーンの取得に失敗しました.."
                return
        if zone_name in self['zone_info']:
            yield "すでに登録済みのゾーンです"
            return
        zone = self.dns_client.add_new_zone({
            "name": zone_name,
            "email": "attakei@gmail.com",
            "description": "my zone",
            "default_ttl": 30000,
        })
        yield {'zone': zone}
        return

    @re_botcmd(pattern=r'(?P<r_name>[0-9a-zA-Z.]+)の(?P<r_type>A|CNAME)レコードに(?P<r_value>[0-9a-zA-Z.]+)(を追加してください)?', template="zone_created")
    def ja_idcf_dns_new_record(self, msg, match):
        r_name = match.group('r_name')
        r_type = match.group('r_type')
        r_value = match.group('r_value')
        yield 'チェック中です...'
        if self['zone_info'] == {}:
            try:
                self['zone_info'] = self.dns_client.fetch_zones()
            except requests.RequestException:
                yield "取得に失敗しました.."
                return
        zone_name = '.'.join(r_name.split('.')[-2:])
        if zone_name not in self['zone_info']:
            yield "管理していないゾーン名ですね"
            return
        zone_info = self['zone_info'][zone_name]
        record = self.dns_client.add_new_record(
            zone_info['uuid'],
            {
                "name": r_name + '.',
                "type": r_type,
                "content": r_value,
                "ttl": zone_info['default_ttl'],
            })
        yield '作成しました'
