# -*- coding:utf8 -*-
import os
import logging
from dotenv import load_dotenv,find_dotenv


load_dotenv(find_dotenv())

HERE = os.path.dirname(os.path.abspath(__file__))

# This is a minimal configuration to get you started with the Text mode.
# If you want to connect Errbot to chat services, checkout
# the options in the more complete config-template.py from here:
# https://raw.githubusercontent.com/errbotio/errbot/master/errbot/config-template.py

BACKEND = 'Text'  # Errbot will start in text mode (console only mode) and will answer commands from there.

BOT_DATA_DIR = os.path.join(HERE, 'data')
BOT_EXTRA_PLUGIN_DIR = os.path.join(HERE, 'plugins')

BOT_LOG_FILE = os.path.join(HERE, 'errbot.log')
BOT_LOG_LEVEL = logging.INFO

BOT_ADMINS = ('CHANGE ME', )  # !! Don't leave that to "CHANGE ME" if you connect your errbot to a chat system !!


BACKEND = 'Slack'  # defaults to XMPP

BOT_IDENTITY = {
    'token': os.environ['SLACK_BOT_TOKEN'],
}

# L179付近
# このErrbotの管理権限をもつアカウントのリスト。@から始めるやつ
BOT_ADMINS = ()
